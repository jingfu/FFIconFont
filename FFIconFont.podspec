Pod::Spec.new do |s|
  s.name     = 'FFIconFont'
  s.version  = '1.9'
  s.license  = 'MIT'
  s.homepage = 'http://git.intra.ffan.com/FFIconFont'
  s.author   = { "ran jingfu" => "ran jingfu@wanda.cn" }
  s.summary  = '<#描述#>'

  s.platform = :ios, '8.0'
  s.source = { :git => 'https://git.oschina.net/jingfu/FFIconFont.git', :tag=>s.version.to_s }
  s.prefix_header_file = 'FFIconFont/Supporting Files/PrefixHeader.pch' 
  s.pod_target_xcconfig = { "__TARGET_NAME__" => "\"$(PRODUCT_NAME)\"" }
  s.source_files = 'FFIconFont/**/*.{h,m,c,mm}'
  s.resources = ["FFIconFont/**/*.ttf"]
  
  # s.dependency '<#WDWorkFlow#>'
end
