//
//  AppDelegate.h
//  FFIconFontDemo
//
//  Created by ran jingfu on 2016/11/23.
//  Copyright © 2016年 ran jingfu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

